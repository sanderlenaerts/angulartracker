<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <jsp:include page="head.jsp"/>
    <body>
        <header>
            <div class="navbar-fixed">
                <nav class="yellow accent-4">
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo center black-text">Activity Tracker</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse black-text">
                            <i class="material-icons">menu</i>
                        </a>
                        <ul id="nav-mobile" class="left hide-on-med-and-down">
                            <li>
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            </li>
                            <li class="active">
                                <a href="<c:url value="/activity.htm"/>"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                                <ul class="right">
                            <c:choose>
                                <c:when test="${cookie.lang == null} ">
                                    <li>
                                        <a href="?lang=nl">Nederlands</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${cookie.lang.value == 'en'}">
                                            <li>
                                                <a href="?lang=nl">Nederlands</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <a href="?lang=en">English</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>

                                </c:otherwise>
                            </c:choose>

                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            <li>
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/>
                                </a>
                            </li>
                            <li class="active">
                                <a href="<c:url value="/activity.htm" />"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/></a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </header>

        <main>
            <nav>
                <div class="nav-wrapper blue darken-3 white-text">
                    <div class="col s12">
                        <div class="start-margin">
                            <a class="breadcrumb" href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            <a class="breadcrumb" href="<c:url value="/person/${person.id}/activities.htm" />">${person.nickname}</a>
                            <a class="breadcrumb" href="<c:url value="" />"><spring:message code="link.addActivity"/></a>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="row">

                    <div class="col s12 l9 center-form">
                        <h5><spring:message code="title.addActivity"/></h5>
                        <h6>${person.firstname} ${person.lastname} - ${person.nickname}</h6>
                        <form:form modelAttribute="activity" id="activity-form" method="POST" action="${pageContext.request.contextPath}/activity/put/${person.id}.htm">
                            <div class="row">
                                <div class="input-field col s6">
                                    <form:errors path="distance" cssClass="error"></form:errors>
                                    <form:input type="number" path="distance" name="distance" placeholder="0" id="distance" class="validate"></form:input>
                                    <form:label path="distance" for="distance"><spring:message code="table.distance"/>
                                        (in meters)</form:label>
                                </div>
                                <div class="input-field col s6">
                                    <form:errors path="time" cssClass="error"></form:errors>
                                    <form:input type="number" path="time" placeholder="0" name="time" id="time" class="validate" value=""></form:input>
                                    <form:label path="time" for="time"><spring:message code="table.time"/>
                                        (in seconds)</form:label>

                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <form:errors cssClass="error" path="date"></form:errors>
                                    <form:input path="date" name="date" placeholder="yyyy-MM-dd" id="date" type="date" class="datepicker" value=""></form:input>
                                    <label path="date" for="date" class="pick-date"><spring:message code="table.date"/>
                                    </label>
                                </div>
                                <div class="col s12">
                                    <a onclick="document.getElementById('activity-form').submit();" class="waves-effect waves-light yellow accent-4 black-text btn">
                                        <i class="material-icons right">done</i><spring:message code="link.addActivity"/></a>
                                </div>
                            </div>

                        </form:form>
                    </div>

                </div>
            </div>
        </main>
        <footer></footer>
    </body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form"
           uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <jsp:include page="head.jsp"/>
    <body>
        <header>
            <div class="navbar-fixed">
                <nav class="yellow accent-4">
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo center black-text">Activity Tracker</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse black-text">
                            <i class="material-icons">menu</i>
                        </a>
                        <ul id="nav-mobile" class="left hide-on-med-and-down">
                            <li>
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            </li>
                            <li>
                                <a href="<c:url value="/activity.htm"/>"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/>
                                </a>
                            </li>
                            <li class="active">
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                                <ul class="right">
                            <c:choose>
                                <c:when test="${cookie.lang == null} ">
                                    <li>
                                        <a href="?lang=nl">Nederlands</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${cookie.lang.value == 'en'}">
                                            <li>
                                                <a href="?lang=nl">Nederlands</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <a href="?lang=en">English</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>

                                </c:otherwise>
                            </c:choose>

                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            <li>
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/activity.htm" />"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/></a>
                            </li>
                            <li class="active">
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </header>

        <main>
            <nav>
                <div class="nav-wrapper blue darken-3 white-text">
                    <div class="col s12">
                        <div class="start-margin">
                            <a class="breadcrumb" href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            <a class="breadcrumb" href="<c:url value="/person/new.htm" />"><spring:message code="link.addPerson"/></a>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="row">

                    <div class="col s12 l9 center-form">
                        <h5><spring:message code="title.register"/></h5>
                        <form:form modelAttribute="person" id="person-form" method="post" action="add.htm">
                            <div class="row">
                                <div class="input-field col s12">
                                    <form:errors path="nickname" cssClass="error"></form:errors>
                                    <label for="nickname"><spring:message code="table.username"/></label>
                                    <form:input path="nickname" name="nickname" placeholder="Nickname" id="nickname" type="text" class="validate" value="${person.nickname}"></form:input>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6">
                                    <form:errors path="firstname" cssClass="error"></form:errors>
                                    <form:input path="firstname" placeholder="Firstname" name="firstname" id="firstname" type="text" class="validate" value="${person.firstname}"></form:input>
                                    <label for="firstname"><spring:message code="table.firstname"/></label>
                                </div>
                                <div class="input-field col s6">
                                    <form:errors path="lastname" cssClass="error"></form:errors>
                                    <form:input path="lastname" placeholder="Lastname" name="lastname" id="lastname" type="text" class="validate" value="${person.lastname}"></form:input>
                                    <label for="lastname"><spring:message code="table.lastname"/></label>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <a onclick="document.getElementById('person-form').submit();" class="waves-effect waves-light yellow accent-4 black-text btn">
                                        <i class="material-icons right">done</i><spring:message code="link.addPerson"/></a>
                                </div>
                            </div>

                        </form:form>
                    </div>

                </div>
            </div>
        </main>
        <footer></footer>
    </body>
</html>

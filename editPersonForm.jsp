<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <jsp:include page="head.jsp"/>
    <body>
        <header>
            <div class="navbar-fixed">
                <nav class="yellow accent-4">
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo center black-text">Activity Tracker</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse black-text">
                            <i class="material-icons">menu</i>
                        </a>
                        <ul id="nav-mobile" class="left hide-on-med-and-down">
                            <li class="active">
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            </li>
                            <li>
                                <a href="<c:url value="/activity.htm"/>"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                                <ul class="right">
                            <c:choose>
                                <c:when test="${cookie.lang == null} ">
                                    <li>
                                        <a href="?lang=nl">Nederlands</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${cookie.lang.value == 'en'}">
                                            <li>
                                                <a href="?lang=nl">Nederlands</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <a href="?lang=en">English</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>

                                </c:otherwise>
                            </c:choose>

                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            <li class="active">
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/activity.htm" />"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/></a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </header>

        <main>
            <nav>
                <div class="nav-wrapper blue darken-3 white-text">
                    <div class="col s12">
                        <div class="start-margin">
                            <a class="breadcrumb" href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            <a class="breadcrumb" href="<c:url value="/person/${person.id}.htm" />"><spring:message code="link.edit"/>
                                ${person.nickname}</a>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="row">

                    <div class="col s12 l9 center-form">
                        <h5><spring:message code="link.edit"/>
                            ${person.nickname}</h5>
                        <form id="person-form" method="POST" action="<c:url value="/person/${person.id}.htm"/> ">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input disabled name="nicknamedis" placeholder="Nickname" id="nicknamedis" type="text" class="validate" value="${person.nickname}">
                                        <input type="hidden" name="nickname" id="nickname" value="${person.nickname}">
                                            <label for="nicknamedis"><spring:message code="table.username"/></label>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input placeholder="Firstname" name="firstname" id="firstname" type="text" class="validate" value="${person.firstname}">
                                                <label for="firstname"><spring:message code="table.firstname"/></label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input placeholder="Lastname" name="lastname" id="lastname" type="text" class="validate" value="${person.lastname}">
                                                    <label for="lastname"><spring:message code="table.lastname"/></label>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col s12">
                                                    <a onclick="document.getElementById('person-form').submit();" class="waves-effect waves-light yellow accent-4 black-text btn">
                                                        <i class="material-icons right">done</i><spring:message code="link.save"/></a>
                                                </div>
                                            </div>

                                        </form>
                                    </div>

                                </div>
                            </div>
                        </main>
                        <footer></footer>
                    </body>
                </html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <jsp:include page="head.jsp"/>
    <body>
        <header>
            <div class="navbar-fixed">
                <nav class="yellow accent-4">
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo center black-text">Activity Tracker</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse black-text">
                            <i class="material-icons">menu</i>
                        </a>
                        <ul id="nav-mobile" class="left hide-on-med-and-down">
                            <li>
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/></a>
                            </li>
                            <li>
                                <a href="<c:url value="/activity.htm"/>"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/>
                                </a>
                            </li>
                            <li class="active">
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                                <ul class="right">
                            <c:choose>
                                <c:when test="${cookie.lang == null} ">
                                    <li>
                                        <a href="?lang=nl">Nederlands</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${cookie.lang.value == 'en'}">
                                            <li>
                                                <a href="?lang=nl">Nederlands</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <a href="?lang=en">English</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>

                                </c:otherwise>
                            </c:choose>

                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            <li>
                                <a href="<c:url value="/person.htm" />"><spring:message code="link.runners"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/activity.htm" />"><spring:message code="link.activities"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/person/login.htm"/>"><spring:message code="link.login"/></a>
                            </li>
                            <li class="active">
                                <a href="<c:url value="/person/new.htm"/>"><spring:message code="link.register"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </header>

        <main>
            <div class="container">
                <div class="row">

                    <div class="col s12 l9 center-form">
                        <h5><spring:message code="title.register"/></h5>
                        <form>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input placeholder="Firstname" id="firstname" type="text" class="validate">
                                        <label for="firstname"><spring:message code="table.firstname"/></label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input placeholder="Lastname" id="lastname" type="text" class="validate">
                                            <label for="lastname"><spring:message code="table.lastname"/></label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input placeholder="Username" id="username" type="text" class="validate">
                                                <label for="username"><spring:message code="table.username"/></label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input placeholder="Password" id="password" type="password" class="validate">
                                                    <label for="password">Password</label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col s12">
                                                    <a class="waves-effect waves-light blue darken-3 btn">
                                                        <i class="material-icons right">done</i><spring:message code="link.register"/></a>
                                                </div>
                                            </div>

                                        </form>
                                    </div>

                                </div>
                            </div>
                        </main>
                        <footer></footer>
                    </body>
                </html>

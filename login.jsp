<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <jsp:include page="head.jsp"/>
    <body>
        <header>
            <div class="navbar-fixed">
                <nav class="yellow accent-4">
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo center black-text">Activity Tracker</a>
                        <a href="#" data-activates="mobile-demo" class="button-collapse black-text">
                            <i class="material-icons">menu</i>
                        </a>
                        <ul id="nav-mobile" class="left hide-on-med-and-down">
                            <li>
                                <a href="#">Runners</a>
                            </li>
                            <li>
                                <a href="activity.jsp">Activities</a>
                            </li>
                            <li class="active">
                                <a href="login.jsp">Login</a>
                            </li>
                            <li>
                                <a href="#">Register</a>
                            </li>
                        </ul>
                        <ul class="right">
                            <c:choose>
                                <c:when test="${cookie.lang == null} ">
                                    <li>
                                        <a href="?lang=nl">Nederlands</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${cookie.lang.value == 'en'}">
                                            <li>
                                                <a href="?lang=nl">Nederlands</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <a href="?lang=en">English</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>

                                </c:otherwise>
                            </c:choose>

                        </ul>
                        <ul class="side-nav" id="mobile-demo">
                            <li>
                                <a href="#">Runners</a>
                            </li>
                            <li>
                                <a href="activity.jsp">Activities</a>
                            </li>
                            <li class="active">
                                <a href="login.jsp">Login</a>
                            </li>
                            <li>
                                <a href="#">Register</a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </header>

        <main>
            <div class="container">
                <div class="row">

                    <div class="col s12 l9 center-form">
                        <h5>Login</h5>
                        <form id="login_form" method="POST" action="Controller?action=login">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="Username" id="username" type="text" class="validate">
                                        <label for="username">Username</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input placeholder="Password" id="password" type="password" class="validate">
                                            <label for="password">Password</label>
                                        </div>
                                        <div class="col s12">
                                            <a class="waves-effect waves-light indigo btn">
                                                <i class="material-icons right" onclick="document.getElementById('login_form').submit()">perm_identity</i>Log in</a>
                                        </div>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </main>
                <footer></footer>
            </body>
        </html>

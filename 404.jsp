<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <jsp:include page="head.jsp"/>

    <body class="yellow accent-4 black-text">
        <main class="valign-wrapper">
            <div class="container valign">
                <h1 class="center-align class404">404</h5>
                <p class="center"><spring:message code="error.404message" /></p>
                <div class="center">
                    <a  class="center-align black yellow-text text-accent-4 btn" href="<c:url value="/index.htm"/>">Home</a>
                </div>
                
            </div>
        </main>
        <footer></footer>
    </body>
</html>
module.exports = function(grunt){

    /**
     * Retrieving current target
     */
    var target = grunt.option('target') || 'dev';
    var availableTargets = [
        'dev',
        'prod'
    ];

    /**
     * Load environment-specific variables
     */
    var envConfig = grunt.file.readJSON('config.' + target + '.json');

    /**
     * This is the configuration object Grunt uses to give each plugin its
     * instructions.
     */

    grunt.initConfig({
        env: envConfig,

        preprocess: {
            options: {
                context: {
                    ENV_WS_URL: '<%= env.wsUrl %>'
                }
            },

            constants: {
                src: 'js/constants.tpl.js',
                dest: 'js/constants.js'
            }
        },

        pkg: grunt.file.readJSON('package.json'),

    	sass: {
    		dist: {
                files: {
                    'css/stylesheet.css' : 'css/stylesheet.sass'
                }
            }

    	}
    });

    grunt.loadNpmTasks('grunt-preprocess');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('run', ['sass', 'preprocess:constants']);


};

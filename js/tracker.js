angular.module('trackerApp', ['ngCookies', 'ui.router', 'pascalprecht.translate', 'trackerApp.config'])

    .run(['$rootScope', '$http', 'BASE', function($rootScope, $http, baseUrl){

            $rootScope.baseUrl = baseUrl;
    }])

    .config(function($stateProvider, $urlRouterProvider, $translateProvider){
        $urlRouterProvider.otherwise('/');


        $translateProvider.translations('en', {
            TITLE: 'Welcome!',
            INTROMESSAGE: 'Use this Activity Tracker application for free!',
            en: 'English',
            nl: 'Nederlands',
            AM_RUNS: 'In total there are {{amount}} runactivities',
            FASTESTRUN_CONTENT: '{{nickname}} is the fastest runner of the pack. {{nickname}} ran {{distance}} km in just about {{time}}.',
            LONGESTRUN_CONTENT: '{{nickname}} rang the longest distance. {{nickname}} ran {{distance}} km in {{time}}.',
            DATE: 'Date',
            DISTANCE: 'Distance',
            TIME: 'Time',
            PACE: 'Pace',
            RUNNER: 'Runner',
            SPEED: 'Speed',
            RUNACTIVITIES: 'Run Activities',
            RUNNERS: 'Runners',
            ADD: 'Add',
            AMOUNTOFRUNS: 'Amount of Runs',
            FASTESTRUN: 'Fastest Run',
            LONGESTRUN: 'Longest Run',
            FASTESTRUNNER: 'Fastest runner',
            LONGESTRUNNER: 'Runner with longest distance',
            USERNAME: 'Username',
            FIRSTNAME: 'Firstname',
            LASTNAME: 'Lastname',
            ACTIVE: 'Active',
            ADDPERSON: 'Add new person',
            FASTESTRUNNER_CONTENT: '{{nickname}} ({{firstname}} {{lastname}}) is the fastest runner of them all. Can you beat {{nickname}}?',
            LONGESTRUNNER_CONTENT: '{{nickname}} ({{firstname}} {{lastname}}) ran the longest distance. Can you beat {{nickname}}?'
        })
        .translations('nl', {
            TITLE: 'Welkom!',
            INTROMESSAGE: 'Gebruik deze volledig gratis activiteitstracker!',
            en: 'English',
            nl: 'Nederlands',
            AM_RUNS: 'In totaal zijn er {{amount}} loopactiviteiten',
            FASTESTRUN_CONTENT: '{{nickname}} is de snelste loper. {{nickname}} liep {{distance}} km in slechts {{time}}.',
            LONGESTRUN_CONTENT: '{{nickname}} liep de langste activiteit. {{nickname}} liep {{distance}} km in {{time}}.',
            DATE: 'Datum',
            DISTANCE: 'Afstand',
            TIME: 'Tijd',
            PACE: 'Tempo',
            RUNNER: 'Loper',
            SPEED: 'Snelheid',
            RUNACTIVITIES: 'Loopactiviteiten',
            RUNNERS: 'Lopers',
            ADD: 'Voeg toe',
            AMOUNTOFRUNS: 'Aantal loopactiviteiten',
            FASTESTRUN: 'Snelste Activiteit',
            LONGESTRUN: 'Langste Activiteit',
            FASTESTRUNNER: 'Snelste loper',
            LONGESTRUNNER: 'Loper met langste afstand',
            USERNAME: 'Gebruikersnaam',
            FIRSTNAME: 'Voornaam',
            LASTNAME: 'Achternaam',
            ACTIVE: 'Actieve',
            ADDPERSON: 'Voeg nieuwe persoon toe',
            FASTESTRUNNER_CONTENT: '{{nickname}} ({{firstname}} {{lastname}}) is de snelste loper van ze allemaal. Kan jij {{nickname}} verslaan?',
            LONGESTRUNNER_CONTENT: '{{nickname}} ({{firstname}} {{lastname}}) liep de langste afstand van iedereen. Heb jij een betere conditie als {{nickname}}?'

        });


        $translateProvider.preferredLanguage('en');



        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/home.html',
                controller: 'weatherController as weather'
            })

            .state('activities', {
                url: '/activities',
                templateUrl: 'views/activity.html',
                controller: 'activityController as activities'
            })

            .state('runners', {
                url: '/runners',
                templateUrl: 'views/runners.html',
                controller: 'personController as persons'

            })

            .state('addPerson', {
                url: '/runners/add',
                templateUrl: 'views/addPerson.html',
                controller: 'addPersonController as person'
            })

            .state('persondetails', {
                url: '/runners/:userId',
                templateUrl: 'views/activitiesByPerson.html',
                controller: 'personDetailController as personDetails'
            });
})

    .controller('addPersonController', function($http){



    })

    .controller('personController', function($http){

        var persons = this;

        $http({
            url: 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/person',
            method: 'GET',
            crossDomain: true


        }).success(function(response){
            persons.personList = response;
        });

        $http({
            url: 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/person/fastest',
            method: 'GET',
            crossDomain: true

        }).success(function(response){
            persons.fastest = response;
        });

        $http({
            url: 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/person/longest',
            method: 'GET',
            crossDomain: true

        }).success(function(response){
            persons.longest = response;
        });

    })
    .controller('activityController', function($http){

        var activities = this;

        $http({
            url: 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/activity',
            method: 'GET'
        }).success(function(response){
            activities.activityList = response;
        });

        $http({
            url: 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/activity/fastest',
            method: 'GET'
        }).success(function(response){
            activities.fastest = response;
        });

        $http({
            url: 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/activity/longest',
            method: 'GET'
        }).success(function(response){
            activities.longest = response;
        });

    })


    .controller('headerController', function ($scope, $location){

            $scope.isActive = function (viewLocation) {
                return viewLocation === $location.path();
            };

    })

    .controller('weatherController', function($http){
        var weather = this;

        weather.searchWeather = function(event){
            if (event.which === 13){
                console.log("Pressed enter");
                var urlRequest = 'http://api.openweathermap.org/data/2.5/forecast/city?q=' + weather.city  + '&APPID=9d930f90124667f8d0a332e7109a21fc&units=metric'
                $http({
                    url: urlRequest,
                    method: 'GET'
                }).success(function(response){
                    weather.weatherStats = response;
                    weather.country = weather.weatherStats.city.country;
                    weather.city = weather.weatherStats.city.name;
                    weather.degrees = weather.weatherStats.list[0].main.temp + " °C";
                    weather.rainChance = weather.weatherStats.list[0].clouds.all + " % chance of rain";
                    weather.description = weather.weatherStats.list[0].weather[0].main;
                    var icon = weather.weatherStats.list[0].weather[0].icon;
                    var date = new Date(weather.weatherStats.list[0].dt * 1000);
                    weather.dateText = date.toLocaleDateString();

                    weather.iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
                });
            }

        }
    })

    .controller('translateController', function($translate, $cookies){
        var trsl = this;

        trsl.language = 'en';
        trsl.languages = ['en', 'nl'];
        trsl.updateLanguage = function(){
            $translate.use(trsl.language);
            $cookies.put('lang', trsl.language);
        };

        if ($cookies.get('lang') != null){
            trsl.language = $cookies.get('lang');
            trsl.updateLanguage();
        }




    })

    .controller('personDetailController', function($http, $stateParams){
        var personDetails = this;
        var userId = $stateParams.userId;



        var reqUrl = 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/person/' + userId + '/activities';

        $http({
            url: reqUrl,
            method: 'GET'


        }).success(function(response){
            personDetails.activities = response;
            var req2Url = 'http://193.191.187.14:10578/RestTracker-1.0-SNAPSHOT/person/' + userId;

            $http({
                url: req2Url,
                method: 'GET'


            }).success(function(response){
                personDetails.person = response;
            });
        });



    });

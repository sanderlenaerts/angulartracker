$(document).ready(function(){
    
    $( "#city-search" ).keydown(function(e) {
        if (e.which === 13){
            $.ajax({
            data: 'json',
            type: 'GET',
            url: "http://api.openweathermap.org/data/2.5/forecast/city?q=" + $('#city-search').val() + "&APPID=9d930f90124667f8d0a332e7109a21fc&units=metric",
            success: function(json){
                alert(json.city.name);
                
                var weatherdata = json;
                
                
                var country = weatherdata.city.country;
                var city = weatherdata.city.name;
                var degrees = weatherdata.list[0].main.temp;
                var rainchance = weatherdata.list[0].clouds.all;
                var description = weatherdata.list[0].weather[0].main;
                var icon = weatherdata.list[0].weather[0].icon;
                
                var iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
                
                
                $('#weather-description').html(description);
                $('#weather-degrees').html(degrees);
                $('#rain-chance').html(rainchance);
                $('#weather-icon').html(iconUrl);
                $('#city-search').val(city + ', ' + country);
                
            }
            });
        }
    });
    
        
    
});